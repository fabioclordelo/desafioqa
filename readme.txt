Desafio 1

Elaborei um plano de testes para o primeiro desafio com três cenários. 
O 1º cenário valida o acesso ao Guia Médico e a pesquisa por Médicos da especialidade Acupuntura na cidade do Rio de Janeiro 
O 2º cenário valida o acesso ao Guia Médico e a pesquisa por Médicos da especialidade Alergia e Imunologia na cidade do Rio de Janeiro 
O 3º cenário valida o resultado da pesquisa por Médicos na cidade do Rio de Janeiro nas páginas 1, 2 e 3, e verifica que não retorno de Médicos de São Paulo

Arquivo com a solução: USXXXXXX - Validar_Acesso_Guia_Medico.xlsx

Desafio 2
Elaborei um plano de testes automatizados para a API ONDb a fim de testar os seguintes cenários:

Cenário de Sucesso (200)
Chamada com os parâmetros “i” e “API_KEY” corretos
Resposta de 200, com os campos Título, Ano e Idioma validados.

Cenário de busca por um filme inexistente (200)
Chamada com o parâmetro API_KEY” corretos, mas com o parâmetro “i” de um filme inexistente no banco de dados.
Resposta de erro

Arquivo com a solução: OMDbAPI.xlsx

Desafio 3
Para este desafio, o plano era criar uma tabela com as informações dadas (Id FIlme, Nme Filme, Gênero e R$) utiliza Java DB. 
O cálculo do desconto seria feito através do seguinte código:

    if(valor > 100) && (valor < 200{
      desconto = 0.1;
    }
    if(valor > 200) && (valor < 300{
      desconto = 0.2;
    }
    if(valor > 300) && (valor < 400{
      desconto = 0.25;
    } 
    if(valor > 400) {
      desconto = 0.3;
    } 
 

A ideia era realizar a consulta dos filmes disponíveis que seriam então adicionados ao carrinho, e através do código acima seria aplicado o desconto de acordo com o valor.

Não consegui seguir com esse desafio por questão de tempo. Como estou atuando em um projeto, surgiram diversas demandas nos últimos dias, principalmente da última sexta até a manhã de ontem, devido a urgência com algumas entregas. Também houve o contratempo de eu ter ficado sem energia em minha residência desde a madrugada e por boa parte do dia de hoje. Só consegui retomar o desenvolvimento agora ao final do dia, mas como eu também tinha outras obrigações, não tive tempo hábil de completar o desafio de número 3. De qualquer forma, estou colocando aqui a ideia de como iria resolver esse desafio e parte do que desenvolvi.